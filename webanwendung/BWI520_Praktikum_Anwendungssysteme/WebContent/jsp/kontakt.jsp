<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="../css/kontakt.css">
<link rel="stylesheet" href="../css/Navigation.css">
</head>
<body>
 
 <div class="main">
        <div class="navbar">
            <div class="icon">
                <h2 class="logo">STUDCOM</h2>
            </div>

            <div class="menu">
                <ul>
                    <li><a href="../jsp/willko.jsp">HOME</a></li>                    
                    <li><a href="#">SERVICE</a></li>
                    <li><a href="#">DOCUMENTATION</a></li>
                    <li><a href="../css/kontakt.css">CONTACT</a></li>
                    <li><a href="#">LOGOUT</a></li>
                </ul>
            </div>

            <div class="search">
                <input class="srch" type="search" name="" placeholder="Type To text">
                <a href="#"> <button class="btn">Search</button></a>
            </div>
 
        </div> 

<div class="container">
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="row">
			<h4 style="text-align:center">We'd love to hear from you!</h4>
	</div>
	<div class="row input-container">
			<div class="col-xs-12">
				<div class="styled-input wide">
					<input type="text" required />
					<label>Name</label> 
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="styled-input">
					<input type="text" required />
					<label>Email</label> 
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="styled-input" style="float:right;">
					<input type="text" required />
					<label>Phone Number</label> 
				</div>
			</div>
			<div class="col-xs-12">
				<div class="styled-input wide">
					<textarea required></textarea>
					<label>Message</label>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="btn-lrg submit-btn">Send Message</div>
			</div>
	</div>
</div>


</body>
</html>